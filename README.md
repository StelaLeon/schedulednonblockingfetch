## Description
A simple service that will poll an endpoint each {configurable interval in seconds} and refreshes the cache from which through a web-interface will deliver the data at a particular index.
 In order to ensure a low memory consumption the data is compressed with run-length-encoding.
  
  The service is available via port 9000 on localhost
  To run the service:
    -verify the application.conf
    -make sure the port is not taken
     
  Useful commands:
    -sbt clean compile test run
    