import org.scalatest.FlatSpec

class CompressionEngineSpec extends FlatSpec {
  "A CompresionEngine" should "return an empty seq if provided an empty seq" in {
    assert(CompressionEngine.compress[Any](Seq.empty[Any]) === Seq.empty[Any])
  }

  it should "compress a single elem correctly" in {
    assert(CompressionEngine.compress[Char](Seq.fill(1)('S')) === Seq(Single('S')))
  }

  it should "compress a single elem seq correctly" in {
    assert(CompressionEngine.compress[Char](Seq.fill(2)('S')) === Seq(Repeat(2,'S')))
  }

  it should "compress a seq correctly" in {
    assert(CompressionEngine.compress[Char](Seq.fill(2)('S') ++ Seq.fill(1)('C') ++ Seq.fill(4)('G'))
      === Seq(Repeat(2,'S'), Single('C'), Repeat(4,'G')))
  }


  it should "decompress a seq correctly" in {
    assert(CompressionEngine.decompress[Char](Seq(Repeat(2,'S'), Single('C'), Repeat(4,'G')))
      === Seq.fill(2)('S') ++ Seq.fill(1)('C') ++ Seq.fill(4)('G'))
  }

  it should "decompress a single elem correctly" in {
    assert(CompressionEngine.decompress[Char](Seq(Single('S')))  === Seq.fill(1)('S')  )
  }

}