import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{FlatSpec, Matchers}

class HttpInterfaceSpec extends FlatSpec with Matchers with ScalatestRouteTest with  HttpInterfaceFetcher{
  "Service" should "respond with a string and 200 OK" in {
    Get(s"/5") ~> routes ~> check {
      status shouldBe OK
      responseAs[String] shouldBe """Done"""
    }
  }

  override def answerProvider(index: Int): Option[String] = Some("Done")
}
