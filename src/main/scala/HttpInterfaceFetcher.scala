import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.marshalling.ToResponseMarshallable

trait HttpInterfaceFetcher {

  def answerProvider(index: Int): Option[String]

  val routes = {
    logRequestResult("http-interface") {
        (get & path(Segment)) { index =>
          complete(
            ToResponseMarshallable(answerProvider(index.toInt).getOrElse("Could not be found"))
          )
        }
      }
  }

}
