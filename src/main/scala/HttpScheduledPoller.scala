 import MainRunner._

 trait ReadableAtIndex[A] {
   def readAtIndex(index:Int, a: Seq[A]): Option[String]
 }

 object ReadableAtIndex {
   def apply[A](implicit sh: ReadableAtIndex[A]): ReadableAtIndex[A] = sh

   def readAtIndex[A: ReadableAtIndex](index:Int, a: Seq[A]) = ReadableAtIndex[A].readAtIndex(index,a)

   implicit val compressedSearchable: ReadableAtIndex[CompressedChars] =
     new ReadableAtIndex[CompressedChars] {
       def readAtIndex(index:Int, inta: Seq[CompressedChars]): Option[String] =
         inta match {
           case Nil => None
           case head:: xs  =>
             head match {
             case a: Single[String] if(index == 0)=> Some(a.element.toString)
             case aa: Single[String] =>readAtIndex(index-1,xs)
             case r: Repeat[String] if(r.count >=index) => Some(r.element.toString)
             case rr: Repeat[String] => readAtIndex(index-rr.count,xs)
             }
         }
     }

   implicit val rawSearchable: ReadableAtIndex[String]=
     new ReadableAtIndex[String] {
       override def readAtIndex(index: Int, a: Seq[String]): Option[String] = Option(a(index).toString)
     }
 }


 class HttpScheduledPoller[A](var value:Seq[A])(implicit readableAtIndex: ReadableAtIndex[A]) {
   def refresh(items: Seq[A]) = {
    value = items
  }
  def fetchAtIndex(index: Int): Option[String] = readableAtIndex.readAtIndex(index,value)

}
