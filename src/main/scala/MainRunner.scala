import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.ConfigFactory

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}



object MainRunner extends App {

  type CompressedChars = Compressed[String]

  implicit val system: ActorSystem = ActorSystem()
  implicit val executor = system.dispatcher
  implicit val materializer : Materializer = ActorMaterializer()

  val config = ConfigFactory.load()
  val logger = Logging(system, getClass)

  val pollUrl = config.getString("scheduled.poller.url")
  val pollInterval = config.getInt("scheduled.poller.interval")

  import ReadableAtIndex._

  var httpScheduledPoller: HttpScheduledPoller[Compressed[String]] = new HttpScheduledPoller[Compressed[String]](Seq.empty[Compressed[String]])

  system.scheduler.schedule(Duration.Zero, pollInterval.seconds, new Runnable { override def run = {
    Http().singleRequest(HttpRequest(uri = pollUrl))
      .flatMap(httpResp =>{
        Unmarshal(httpResp.entity)
          .to[String]
          .map(_.split("\n").toList)
      }).onComplete{
      case Success(items) => httpScheduledPoller.refresh(CompressionEngine.compress[String](items))
      case Failure(t) => logger.error("Failure when polling the endpoint")
    }
    }
  })

  val httpInterface = new HttpInterfaceFetcher {
    def answerProvider(index: Int): Option[String] = httpScheduledPoller.fetchAtIndex(index)
  }

  Http().bindAndHandle(httpInterface.routes, "0.0.0.0", 9000) map { result =>
    logger.info("Server has started on port 9000")
  } recover {
    case ex: Exception => logger.error(s"Server binding failed due to ${ex.getMessage}")
  }

}
