
trait Compressor {
  def compress[A]: Seq[A] => Seq[Compressed[A]]
  def decompress[A]: Seq[Compressed[A]] => Seq[A]
}

sealed trait Compressed[+A]{
  val element:A
  def compose[A](that: Compressed[A]):Compressed[A]

}
case class Single[A](element: A) extends Compressed[A]{
  def compose[A](that: Compressed[A]): Compressed[A] = that match{
    case a: Single[A] if(element == a.element) => Repeat(2,a.element)
    case r: Repeat[A] if(element == r.element)=> Repeat[A](r.count+1, r.element)
    case _ => throw new RuntimeException("You are trying to concatenate elements not meant to be concatenated")
  }

}
case class Repeat[A](count: Int, element: A) extends Compressed[A] {
  def compose[A](that: Compressed[A]): Compressed[A] = that match{
    case a: Single[A] if(element == a.element) => Repeat(count+1,a.element)
    case r: Repeat[A] if(element == r.element)=> Repeat[A](r.count+count, r.element)
    case _ => throw new RuntimeException("You are trying to concatenate elements not meant to be concatenated")
  }
}

object CompressionEngine extends Compressor{

  def compress[A]: (Seq[A]) => Seq[Compressed[A]] = {
    def comp(vs: Seq[A],prevElem:Option[Compressed[A]], compressedSeq : Seq[Compressed[A]]) : Seq[Compressed[A]] = {
      (vs, prevElem) match{
        case (Nil,_) => compressedSeq
        case (Seq(head), None) => compressedSeq :+ Single(head)
        case (Seq(head), Some(pE)) =>
          if(pE.element==head)
            compressedSeq :+ pE.compose[A](Single[A](head))
          else
            compressedSeq :+ pE :+ Single(head)
        case (head::tail, Some(prevE)) => prevE match{
          case Single(e) if(e == head) => comp(tail,Some(Repeat(2, head)),compressedSeq)
          case Repeat(cnt, e) if(e == head) => comp(tail, Some(Repeat(cnt+1,e)),compressedSeq)
          case _ => comp(tail, Some(Single(head)),compressedSeq :+ prevE)
        }
        case (head::tail, None) => comp(tail, Some(Single(head)), compressedSeq)
      }
    }

    (vs: Seq[A])=> comp(vs,None, Seq.empty[Compressed[A]])
  }

  def decompress[A]: (Seq[Compressed[A]]) => Seq[A] = {
    (dvs: Seq[Compressed[A]])=>{
      flatten[A](dvs.flatMap{elem=>{
        elem match {
          case a: Single[A] => Seq(a.element)
          case r: Repeat[A] => Seq.fill(r.count)(r.element)
        }
      }})
    }
  }

  //I need a A => GenTraversableOnce
  def flatten[A](ls: Seq[A]): Seq[A] = ls flatMap {
    case i: Seq[A] => flatten[A](i)
    case e: A => Seq[A](e)
  }
}
